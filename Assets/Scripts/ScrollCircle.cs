﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollCircle : ScrollRect
{
    // 摇杆输出的方向向量
    public Vector2 ForceVector {
        get {
            return m_ForceVector;
        }
        private set { }
    }
    

    protected override void Start()
    {
        base.Start();
        m_ForceVector = Vector2.zero;
        // mRadius = (transform as RectTransform).sizeDelta.x * 0.5f; // 计算摇杆块的半径
        m_Radius = 50f;
    }

    public override void OnDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {

        base.OnDrag(eventData);
        var contentPostion = this.content.anchoredPosition;
        if (contentPostion.magnitude > m_Radius)
        {
            contentPostion = contentPostion.normalized * m_Radius;
            SetContentAnchoredPosition(contentPostion);
        }
        m_ForceVector = contentPostion.normalized;
        Debug.Log("方向向量：" + m_ForceVector);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        m_ForceVector = Vector2.zero;
    }

    private Vector2 m_ForceVector;
    protected float m_Radius;
}